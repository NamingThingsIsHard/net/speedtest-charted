Speedtest Charted

A simple, dumb script to create HTML charts of a series of [speedtest][st] readings to show your ISP

**Warning**

When using this, your are implicitly accepting speedtest's 
[EULA][st_eula], [privacy notice][st_privacy] and [TOS][st_tos]

# Quickstart

This requires **Python >=3.6** and **docker**.

```shell
pip install poetry
poetry install
.venv/bin/activate
python main.py /tmp/speedtest/data /tmp/speedtest/html
xdg-open /tmp/speedtest/index.html
```

# Inner workings

Uses [tianon/speedtest][docker_speedtest] docker image to query speedtest.net.
The results are saved in a data directory and an `index.html` is generated in an output directory.
The HTML file includes only the essential data (date with download and upload speed in Mb) 
 displayed in a graph using [chart.js].

There's a split of the data and HTML in order to allow the user to place HTML file into /var/www/html
 and not share additional information (internal IP, external IP, etc.).


[chart.js]: https://www.chartjs.org
[docker_speedtest]: https://hub.docker.com/r/tianon/speedtest

[st]: https://speedtest.net
[st_eula]: https://www.speedtest.net/about/eula 
[st_tos]: https://www.speedtest.net/about/terms
[st_privacy]: https://www.speedtest.net/about/privacy
