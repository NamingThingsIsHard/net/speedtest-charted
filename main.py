"""
fast.com_charted
Copyright (C) 2020 LoveIsGrief

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import argparse
import json
import logging
import shutil
from argparse import ArgumentParser
from pathlib import Path

import docker
import jinja2
from docker.errors import ContainerError

DATA_FILENAME = "speedtests.json"
FILE_DIR = Path(__file__).parent
TEMPLATE_PATH = FILE_DIR / "index.html.jinja"
RESOURCES_PATH = FILE_DIR / "resources"
SPEEDTEST_IMAGE = "tianon/speedtest"


def main(data_dir: Path, output_dir: Path, regenerate=False):
    l = logging.getLogger("speedtest_charted")

    data_file = data_dir / DATA_FILENAME
    speedtests = json.loads(data_file.read_text()) if data_file.is_file() else []

    if not regenerate:
        l.info("Calling speedtest")
        client = docker.from_env()
        if not client.images.list(SPEEDTEST_IMAGE):
            l.info("Pulling image")
            client.images.pull(SPEEDTEST_IMAGE)
        try:
            stdout = client.containers.run(SPEEDTEST_IMAGE, command=[
                "--accept-license",
                "--accept-gdpr",
                "--format=json",
            ], auto_remove=True)
        except ContainerError as container_error:
            l.error("Error running speedtest: %s", container_error)
            raise SystemExit(1)

        # Update collected data with new data
        speedtest_json = json.loads(stdout)
        speedtests.append(speedtest_json)
        l.info("Updating old data with new measurements")
        data_file.write_text(json.dumps(speedtests, indent=2))

    labels, downloads, uploads = generate(speedtests)

    template = jinja2.Template(TEMPLATE_PATH.read_text())
    rendered = template.render(
        labels=labels,
        downloadSeries=downloads,
        uploadSeries=uploads,
    )
    index_html = output_dir / "index.html"
    l.info("Writing html to %s", index_html)
    index_html.write_text(rendered)

    # Copy resources
    l.info("Copying resources to %s", output_dir)
    for item in RESOURCES_PATH.iterdir():
        target = output_dir / item.name
        if target.exists():
            continue
        shutil.copy(str(item), target)

    print("Done!")

def generate(speedtests):
    labels = []
    downloads = []
    uploads = []
    for speedtest in speedtests:
        labels.append(speedtest["timestamp"])
        downloads.append(bytes_to_mbits(speedtest["download"]["bandwidth"]))
        uploads.append(bytes_to_mbits(speedtest["upload"]["bandwidth"]))

    return labels, downloads, uploads


def bytes_to_mbits(byte, precision=2):
    return round(byte * 8 / 1000000, precision)


def accessible_directory_type(path):
    path = Path(path).resolve()
    if path.is_file():
        raise argparse.ArgumentTypeError("Path is a file")

    if not path.is_dir():
        path.mkdir(parents=True, exist_ok=True)

    return path


if __name__ == '__main__':
    logging.basicConfig(
        level=logging.INFO
    )
    parser = ArgumentParser(
        description="Generates an index.html with a graph of speedtest.com readings")
    parser.add_argument(
        "-r", "--regenerate",
        default=False,
        action="store_true",
        help="Regenerates index.html"
    )
    parser.add_argument("data_dir", type=accessible_directory_type)
    parser.add_argument("output_dir", type=accessible_directory_type)

    args = parser.parse_args()
    main(args.data_dir, args.output_dir, regenerate=args.regenerate)
